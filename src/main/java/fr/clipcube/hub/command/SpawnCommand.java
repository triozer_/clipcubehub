package fr.clipcube.hub.command;

import fr.clipcube.api.spigot.command.AbstractCommand;
import fr.clipcube.api.spigot.player.ClipPlayer;

import java.util.List;

/**
 * @author Triozer
 */
public class SpawnCommand extends AbstractCommand {
    public SpawnCommand() {
        super("spawn", null, true, 0);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        sender.teleport(sender.getWorld().getSpawnLocation());
    }

    @Override
    public void displayHelp(ClipPlayer sender) {

    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        return null;
    }
}
