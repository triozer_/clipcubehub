package fr.clipcube.hub.command;

import fr.clipcube.api.spigot.command.AbstractCommand;
import fr.clipcube.api.spigot.player.ClipPlayer;

import java.util.List;

/**
 * @author Triozer
 */
public class GoToCommand extends AbstractCommand {
    public GoToCommand() {
        super("goto", "clip.goto", true, 1);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        sender.sendVoid();
        sender.sendMessage("TELEPORTATION TO: ");
        sender.sendTo(args[0]);
        sender.sendVoid();
    }

    @Override
    public void displayHelp(ClipPlayer sender) {
        sender.sendVoid();
        sender.sendMessage("");
        sender.sendVoid();
    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        return null;
    }
}
