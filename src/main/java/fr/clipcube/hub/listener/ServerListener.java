package fr.clipcube.hub.listener;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.util.Text;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * @author Triozer
 */
public class ServerListener implements Listener {
    @EventHandler
    public void onPing(ServerListPingEvent event) {
        event.setMotd(Text.DEFAULT_MOTD + "\n§6" + ClipAPI.getInstance().getClipServer().getId());
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onSaturation(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onMobSpawn(SpawnerSpawnEvent event) {
        event.setCancelled(true);
    }
}
