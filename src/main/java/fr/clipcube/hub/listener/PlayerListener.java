package fr.clipcube.hub.listener;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.i18n.SimplyTranslation;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.scoreboard.IndividualScoreboard;
import fr.clipcube.api.spigot.util.HologramBuilder;
import fr.clipcube.api.spigot.util.InventoryBuilder;
import fr.clipcube.api.spigot.util.ItemBuilder;
import fr.clipcube.api.spigot.util.Text;
import fr.clipcube.hub.manager.GuiManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

/**
 * @author Triozer
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(event.getPlayer());

        Location location = clipPlayer.getWorld().getSpawnLocation();

        location.setYaw(-90.0f);
        location.setPitch(0f);

        clipPlayer.teleport(location);

        clipPlayer.make(new IndividualScoreboard(clipPlayer.getPlayer(), Text.SCOREBOARD_HEADER));

        clipPlayer.getIndividualScoreboard().setLine(0, "§1");
        clipPlayer.getIndividualScoreboard().setLine(1, clipPlayer.translate("scoreboard.name", clipPlayer.getName()));
        clipPlayer.getIndividualScoreboard().setLine(2, clipPlayer.translate("scoreboard.rank",
                SimplyTranslation.translateRank(clipPlayer, clipPlayer.getRank())));
        clipPlayer.getIndividualScoreboard().setLine(3, clipPlayer.translate("scoreboard.lang", clipPlayer.getLanguage().getLang()));
        clipPlayer.getIndividualScoreboard().setLine(4, "§2");
        clipPlayer.getIndividualScoreboard().setLine(5, "Coins: §a" + clipPlayer.getCoins());
        clipPlayer.getIndividualScoreboard().setLine(6, "§3");
        clipPlayer.getIndividualScoreboard().setLine(7, clipPlayer.translate("scoreboard.server-id", clipPlayer.getClipServer().getId()));
        clipPlayer.getIndividualScoreboard().setLine(8, "§4");
        clipPlayer.getIndividualScoreboard().setLine(9, Text.SCOREBOARD_FOOTER);

        clipPlayer.sendTablist(Text.TABLIST_HEADER + "\n", "\n" + Text.TABLIST_FOOTER);

        event.setJoinMessage("");

        if (clipPlayer.getRank().getId() > 0)
            for (ClipPlayer players : ClipAPI.getPlayerManager().getPlayers())
                players.sendTranslatedMessage("hub.join", SimplyTranslation.translateClipName(players, clipPlayer));

        if (clipPlayer.firstTime) {
            clipPlayer.freeze(10);

            new HologramBuilder(clipPlayer.getLocation().add(0, 0.75, 0)).item(new ItemStack(Material.DIAMOND)).time(10).build();
            new HologramBuilder(clipPlayer.getLocation().add(0, 0.85, 0)).lines("§bNew player !").time(10).build();

            clipPlayer.sendCenteredMessage("§fIt's your first time on the " + Text.SCOREBOARD_HEADER + "§f. ");
        }

        GuiManager.hub(clipPlayer);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage("");
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        event.setCancelled(true);

        if (event.getItem() == null || !event.getItem().hasItemMeta())
            return;

        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(event.getPlayer());

        if (event.getItem().getItemMeta().getDisplayName().contains("MENU")) {
            clipPlayer.openInventory(new InventoryBuilder("                 §6§lMENU", 9 * 6)
                    .setItem(12, new ItemBuilder(Material.STAINED_CLAY).data((byte) 7).name("§6§lCLASH OF CLANS")
                            .lore("", "§a§lONLINE", "", "§r§7ClashOfClans on Minecraft.").build())
                    .setItem(13, new ItemBuilder(Material.STAINED_CLAY).data((byte) 7).name("§6§lSPACECRAFT")
                            .lore("", "§4§l✖ §f§lIN DEV §4§l✖", "", "§r§7Upgrade your planet by accomplish the achievement!").build())
                    .setItem(14, new ItemBuilder(Material.STAINED_CLAY).data((byte) 7).name("§6§l?")
                            .lore("", "§4§l✖ §f§lIN DEV §4§l✖", "", "§r§7?").build())
                    .fillLine(new ItemBuilder(Material.STAINED_GLASS_PANE).data((byte) 7).name(" ").build(), 9 * 3)
                    .build(ClipAPI.getInstance()));
        } else if (event.getItem().getItemMeta().getDisplayName().contains("PROFIL")) {
            clipPlayer.openInventory(new InventoryBuilder("                §6§lPROFIL", 9 * 5)
                    .fill(new ItemBuilder(Material.STAINED_GLASS_PANE).data((byte) 7).name(" ").build())
                    .build(ClipAPI.getInstance()));
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onSwap(PlayerSwapHandItemsEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onClickInventory(InventoryClickEvent event) {
        event.setCancelled(true);

        if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta())
            return;

        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer((Player) event.getWhoClicked());

        if (event.getClickedInventory().getName().contains("MENU")) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().contains("CLASH OF CLANS")) {
                clipPlayer.sendTo("clash-of-clans");
            } else if (event.getCurrentItem().getItemMeta().getDisplayName().contains("SPACECRAFT")) {
                clipPlayer.sendMessage("§l[SPACECRAFT] §4§l✖ §f§lIN DEV §4§l✖");
                clipPlayer.sendTo("space-craft");
            } else if (event.getCurrentItem().getItemMeta().getDisplayName().contains("[?")) {
                clipPlayer.sendMessage("§l[?] §4§l✖ §f§lIN DEV §4§l✖");
                clipPlayer.sendTo("?");
            }
        }
    }

    @EventHandler
    public void onInteractInventory(InventoryInteractEvent event) {
        event.setCancelled(true);
    }

}