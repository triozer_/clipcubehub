package fr.clipcube.hub.manager;

import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.util.InventoryBuilder;
import fr.clipcube.api.spigot.util.ItemBuilder;
import fr.clipcube.api.spigot.util.NpcBuilder;
import fr.clipcube.hub.ClipHub;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Triozer
 */
public class NpcManager {

    private static List<NpcBuilder> npcs = new ArrayList<>();

    public static void init() {
        World world = Bukkit.getWorld("world");

        NpcBuilder lobby = new NpcBuilder(new Location(world, 278.5, 71, 481.5))
                .name("§6§lLOBBY", "", "§fRight click for open gui.")
                .addListener(new NpcBuilder.NpcListener() {
                    @Override
                    public void onClick(PlayerInteractEntityEvent event, ClipPlayer clipPlayer) {
                        super.onClick(event, clipPlayer);

                        clipPlayer.openInventory(new InventoryBuilder("                §6§lLOBBY", 27)
                                .fill(new ItemBuilder(Material.STAINED_GLASS_PANE).name(" ").data((byte) 7).build())
                                .build(ClipHub.getInstance()));
                    }
                }).build();

        NpcBuilder commingSoon = new NpcBuilder(new Location(world, 280.5, 71, 481.5))
                .name("§6§lCLASH OF CLANS", "", "§a§lEN LIGNE")
                .addListener(new NpcBuilder.NpcListener() {
                    @Override
                    public void onClick(PlayerInteractEntityEvent event, ClipPlayer clipPlayer) {
                        super.onClick(event, clipPlayer);

                        clipPlayer.sendTo("clash-of-clans");
                    }
                }).build();

        NpcBuilder chickenHunter = new NpcBuilder(new Location(world, 280.5, 71, 484.5))
                .name("§6§lSPACECRAFT", "", "§4§l✖ §f§lIN DEV §4§l✖")
                .addListener(new NpcBuilder.NpcListener() {
                    @Override
                    public void onClick(PlayerInteractEntityEvent event, ClipPlayer clipPlayer) {
                        super.onClick(event, clipPlayer);

                        clipPlayer.sendMessage("§l[SPACECRAFT] §4§l✖ §f§lIN DEV §4§l✖");
                    }
                }).build();

        NpcBuilder commingSoon1 = new NpcBuilder(new Location(world, 278.5, 71, 484.5))
                .name("§6§l?", "", "§4§l✖ §f§lIN DEV §4§l✖")
                .addListener(new NpcBuilder.NpcListener() {
                    @Override
                    public void onClick(PlayerInteractEntityEvent event, ClipPlayer clipPlayer) {
                        super.onClick(event, clipPlayer);

                        clipPlayer.sendMessage("§l[?] §4§l✖ §f§lIN DEV §4§l✖");
                    }
                }).build();

        npcs.add(lobby);
        npcs.add(chickenHunter);
        npcs.add(commingSoon);
        npcs.add(commingSoon1);

        lookAtSpawn();
    }

    private static void lookAtSpawn() {
        Location spawn = Bukkit.getWorld("world").getSpawnLocation();
        Location loc;

        for (NpcBuilder npcBuilder : npcs) {
            loc = npcBuilder.getVillager().getLocation();

            loc.setDirection(spawn.getDirection().subtract(loc.getDirection()));

            npcBuilder.getVillager().teleport(loc);
        }

    }

    public static List<NpcBuilder> getNpcs() {
        return npcs;
    }
}
