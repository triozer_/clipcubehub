package fr.clipcube.hub.manager;

import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.util.ItemBuilder;
import org.bukkit.Material;

/**
 * @author Triozer
 */
public class GuiManager {

    public static void hub(ClipPlayer clipPlayer) {
        clipPlayer.getInventory().setItem(0, new ItemBuilder(Material.COMPASS).name("§6§lMENU").build());
        clipPlayer.getInventory().setItem(1, new ItemBuilder(Material.NETHER_STAR).name("§6§lPROFIL").build());
    }

}
