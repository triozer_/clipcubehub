package fr.clipcube.hub;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.proxy.ClipServer;
import fr.clipcube.api.spigot.util.NpcBuilder;
import fr.clipcube.hub.command.GoToCommand;
import fr.clipcube.hub.command.SpawnCommand;
import fr.clipcube.hub.listener.PlayerListener;
import fr.clipcube.hub.listener.ServerListener;
import fr.clipcube.hub.manager.NpcManager;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Triozer
 */
public class ClipHub extends JavaPlugin implements Listener {

    private static ClipHub instance;

    public static ClipHub getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;

        ClipAPI.getInstance().initClipServer(new ClipServer((CraftServer) Bukkit.getServer()));

        NpcManager.init();

        registerCommands();
        registerListeners();
    }

    @Override
    public void onDisable() {
        NpcManager.getNpcs().stream().forEach(NpcBuilder::end);
    }

    private void registerCommands() {
        new SpawnCommand().register(this);
        new GoToCommand().register(this);
    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new ServerListener(), this);
    }
}
